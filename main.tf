provider "aws" {
    region = "eu-west-3"
}

variable "cidr_blocks" {
    description = "cidr blocks and name tags for vpc and subnets"
    type = list(object({
        cidr_block = string
        name = string
    }))
}

variable avail_zone {}


variable "vpc_cidr_block" {
    description = "vpc cidr bolck"
}

variable "environment" {
    description = "deployment environment"
}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        Name: var.cidr_blocks[0].name
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: var.cidr_blocks[1].name
    }
}

# Output values
output "dev_vpc_id" {
    value = aws_vpc.development-vpc.id
}
 
output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}

# query existing resources
# data "aws_vpc" "existing_vpc" {
#     # pass in a filter criteria
#     default = true
# }

# create a subnet in that vpc that we have queried
# resource "aws_subnet" "dev-subnet-2" {
#     vpc_id = data.aws_vpc.existing_vpc.id
#     cidr_block = "172.31.48.0/20"
#     availability_zone = "eu-west-3a"
#     tags = {
#         Name: "subnet-2-default"
#     }
# }
